package com.itau.investimentos.services;

import com.itau.investimentos.DTOs.RendimentoMensalDTO;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    SimulacaoService simulacaoService;

    public Investimento criarInvestimento (Investimento investimento){
        Investimento objetoInvestimento = investimentoRepository.save(investimento);

        return objetoInvestimento;
    }

    public Iterable<Investimento> lerListaDeInvestimentos () {
        return investimentoRepository.findAll();
    }

    public Investimento buscarInvestimentoPeloId (int id){
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);

        if(investimentoOptional.isPresent()){
            Investimento investimento= investimentoOptional.get();
            return investimento;
        } else {
            throw new RuntimeException("Esse tipo de investimento não existe");
        }
    }

    //TO DO CRIAR METODOS PARA ATUALIZAR E DELETAR O INVESTIMENTO

    public RendimentoMensalDTO realizarSimulacao (Simulacao simulacao, int id){
        Simulacao simulacaoObjeto = simulacaoService.criarSimulacao(simulacao);
        Investimento investimento = buscarInvestimentoPeloId(id);
        simulacaoObjeto.setInvestimento(investimento);

        RendimentoMensalDTO rendimentoMensalDTO= simulacaoService.calcularSimulacao(simulacao);

        return rendimentoMensalDTO;
    }
}
