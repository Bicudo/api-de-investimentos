package com.itau.investimentos.services;

import com.itau.investimentos.DTOs.RendimentoMensalDTO;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.repositories.InvestimentoRepository;
import com.itau.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public Simulacao criarSimulacao (Simulacao simulacao){
        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);
        return simulacaoObjeto;
    }

    public RendimentoMensalDTO calcularSimulacao (Simulacao simulacaoObjeto){
        double capital = simulacaoObjeto.getValorAplicado();
        double rendimentoPorcentagem= simulacaoObjeto.getInvestimento().getRendimentoAoMes();
        double rendimentoDinheiro = simulacaoObjeto.getValorAplicado()*rendimentoPorcentagem;
        double base = 1+rendimentoPorcentagem;

        double montante = capital*Math.pow(base,simulacaoObjeto.getQtdMeses());

        RendimentoMensalDTO rendimentoMensalDTO = new RendimentoMensalDTO();
        rendimentoMensalDTO.setMontante(montante);
        rendimentoMensalDTO.setRendimento(rendimentoDinheiro);

        return  rendimentoMensalDTO;
    }

    public Iterable<Simulacao> lerTodasSimulacoes (){
        return simulacaoRepository.findAll();
    }
}
