package com.itau.investimentos.repositories;

import com.itau.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

    
}
