package com.itau.investimentos.controllers;

import com.itau.investimentos.DTOs.RendimentoMensalDTO;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    InvestimentoService investimentoService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento criarInvestimentos (@RequestBody Investimento investimento){
        Investimento objetoInvestimento = investimentoService.criarInvestimento(investimento);
        return objetoInvestimento;
    }

    @GetMapping
    public Iterable<Investimento> lerListaDeInvestimentos (){
        return investimentoService.lerListaDeInvestimentos();
    }

    @GetMapping ("/{id}")
    public Investimento buscarPeloID (@PathVariable(name = "id") int id){
        try{
            Investimento investimento = investimentoService.buscarInvestimentoPeloId(id);
            return investimento;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public RendimentoMensalDTO realizarSimulacao (@RequestBody Simulacao simulacao, @PathVariable(name = "id") int id){
        try{
            RendimentoMensalDTO rendimentoMensalDTO = investimentoService.realizarSimulacao(simulacao, id);
            return rendimentoMensalDTO;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
