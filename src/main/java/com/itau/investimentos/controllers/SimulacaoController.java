package com.itau.investimentos.controllers;

import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.services.InvestimentoService;
import com.itau.investimentos.services.SimulacaoService;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {

    @Autowired
    SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Simulacao criarSimulacao(@RequestBody Simulacao simulacao){
        Simulacao objetoSimulacao = simulacaoService.criarSimulacao(simulacao);
        return simulacao;
    }

    @GetMapping
    public Iterable<Simulacao> lerTodasSimulacoes(){
        return simulacaoService.lerTodasSimulacoes();
    }
}
