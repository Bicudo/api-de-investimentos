package com.itau.investimentos.DTOs;

import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;

import javax.persistence.CascadeType;
import javax.persistence.ManyToOne;

public class RendimentoMensalDTO {
    private double montante;
    private double rendimento;

    public RendimentoMensalDTO() {
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }
}
