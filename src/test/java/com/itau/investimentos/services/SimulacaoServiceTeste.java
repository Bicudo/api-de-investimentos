package com.itau.investimentos.services;

import com.itau.investimentos.DTOs.RendimentoMensalDTO;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.repositories.SimulacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Iterator;

@SpringBootTest
public class SimulacaoServiceTeste {


    @MockBean
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private SimulacaoService simulacaoService;

    Simulacao simulacao;
    Investimento investimento;

    @BeforeEach
    private void setUp (){
        this.investimento = new Investimento();
        investimento.setRendimentoAoMes(0.05);
        investimento.setId(1);
        investimento.setNome("POUPANCA");

        this.simulacao = new Simulacao();
        simulacao.setId(1);
        simulacao.setEmail("ingrid.bicudo@usp.br");
        simulacao.setNomeInteressado("Ingrid Monalisa Bicudo");
        simulacao.setQtdMeses(2);
        simulacao.setValorAplicado(2000);
        simulacao.setInvestimento(investimento);
        simulacao.setMontante(2205);
    }

    @Test
    public void testarCriarInvestimento (){
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).then(objeto -> objeto.getArgument(0));

        Simulacao simulacaoTeste = simulacaoService.criarSimulacao(simulacao);

        Assertions.assertEquals(simulacao.getNomeInteressado(), simulacaoTeste.getNomeInteressado());
    }


    @Test
    public void testarCalcularInvestimento (){
        Mockito.when(simulacaoRepository.save(Mockito.any(Simulacao.class))).then(objeto -> objeto.getArgument(0));

        RendimentoMensalDTO calculoTeste = simulacaoService.calcularSimulacao(simulacao);

        Assertions.assertEquals(simulacao.getMontante(), calculoTeste.getMontante());
    }
}
