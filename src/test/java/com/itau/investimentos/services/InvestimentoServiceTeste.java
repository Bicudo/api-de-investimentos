package com.itau.investimentos.services;

import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestExecutionListeners;

import java.util.Optional;

@SpringBootTest
public class InvestimentoServiceTeste {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    Investimento investimento;

    @BeforeEach
    private void setUp(){
        this.investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("POUPANCA");
        investimento.setRendimentoAoMes(0.05);
    }

    @Test
    public void testarCriarInvestimento (){
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).then(objeto -> objeto.getArgument(0));
        Investimento investimentoTeste = investimentoService.criarInvestimento(investimento);

        Assertions.assertEquals(investimento.getNome(), investimentoTeste.getNome());
    }

    @Test
    public void testarBuscaPeloId (){
        Optional<Investimento> investimentoOptional = Optional.of(investimento);
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);

        Investimento investimentoTeste = investimentoService.buscarInvestimentoPeloId(12);

        Assertions.assertEquals(investimento.getNome(), investimentoTeste.getNome());

    }

    @Test
    public void testarBuscaPeloIdQueNaoExiste (){
        Optional<Investimento> investimentoOptional = Optional.empty();
        Mockito.when(investimentoRepository.findById(Mockito.anyInt())).thenReturn(investimentoOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.buscarInvestimentoPeloId(12);});

    }


}
