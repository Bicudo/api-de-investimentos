package com.itau.investimentos.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.models.Simulacao;
import com.itau.investimentos.services.InvestimentoService;
import com.itau.investimentos.services.SimulacaoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest
public class SimulacaoControllerTeste {

    @MockBean
    private SimulacaoService simulacaoService;

    @MockBean
    private InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    Simulacao simulacao;
    Investimento investimento;

    @BeforeEach
    private void setUp (){
        this.investimento = new Investimento();
        investimento.setRendimentoAoMes(0.05);
        investimento.setId(1);
        investimento.setNome("POUPANCA");

        this.simulacao = new Simulacao();
        simulacao.setId(1);
        simulacao.setEmail("ingrid.bicudo@usp.br");
        simulacao.setNomeInteressado("Ingrid Monalisa Bicudo");
        simulacao.setQtdMeses(2);
        simulacao.setValorAplicado(2000);
        simulacao.setInvestimento(investimento);
        simulacao.setMontante(2205);
    }

    @Test
    public void testarCriarSimulacao () throws Exception {
        Mockito.when(simulacaoService.criarSimulacao(Mockito.any(Simulacao.class))).thenReturn(simulacao);
        ObjectMapper objectMapper = new ObjectMapper();

        String simulacaoJson = objectMapper.writeValueAsString(simulacao);

        mockMvc.perform(MockMvcRequestBuilders.post("/simulacao")
                .contentType(MediaType.APPLICATION_JSON).content(simulacaoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeInteressado", CoreMatchers.equalTo("Ingrid Monalisa Bicudo")));
    }

}
