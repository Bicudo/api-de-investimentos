package com.itau.investimentos.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itau.investimentos.models.Investimento;
import com.itau.investimentos.services.InvestimentoService;
import com.itau.investimentos.services.SimulacaoService;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest
public class InvestimentoControllerTeste {

    @MockBean
    private InvestimentoService investimentoService;

    @MockBean
    private SimulacaoService simulacaoService;

    @Autowired
    private MockMvc mockMvc;

    Investimento investimento;

    @BeforeEach
    private void setUp(){
        this.investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("POUPANCA");
        investimento.setRendimentoAoMes(0.05);
    }

    @Test
    public void testarCriarInvestimento () throws Exception {
        Mockito.when(investimentoService.criarInvestimento(Mockito.any(Investimento.class))).thenReturn(investimento);
        ObjectMapper objectMapper = new ObjectMapper();

        String investimentoJson = objectMapper.writeValueAsString(investimento);

        mockMvc.perform(MockMvcRequestBuilders.post("/investimentos")
                .contentType(MediaType.APPLICATION_JSON).content(investimentoJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("POUPANCA")));
    }

    @Test
    public void testarBuscarPeloId () throws Exception {
        Mockito.when(investimentoService.buscarInvestimentoPeloId(Mockito.anyInt())).thenReturn(investimento);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("POUPANCA")));

    }

    @Test
    public void testarBuscarPeloIdQueNaoExiste () throws Exception {
        Mockito.when(investimentoService.buscarInvestimentoPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/investimentos/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }
}
